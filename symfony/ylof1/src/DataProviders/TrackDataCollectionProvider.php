<?php

namespace App\DataProviders;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Track\Track;
use App\Model\TrackModel;
use App\Repository\TrackRepository;

class TrackDataCollectionProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var TrackRepository
     */
    private TrackRepository $repository;

    public function __construct(TrackRepository $repository)
    {
        $this->repository = $repository;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return TrackModel::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        /** @var Track[] $tracks */
        $tracks = $this->repository->findAll();

        foreach ($tracks as $track) {
            yield (new TrackModel())->setId($track->getId())->setName($track->getName());
        }
    }
}