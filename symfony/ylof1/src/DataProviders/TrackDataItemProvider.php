<?php

namespace App\DataProviders;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\Driver\Driver;
use App\Entity\LapTime\RealLapTime;
use App\Entity\Track\Track;
use App\Model\LapTimeModel;
use App\Model\TrackModel;
use App\Repository\TrackRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;

class TrackDataItemProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var TrackRepository
     */
    private TrackRepository $repository;

    /**
     * @var TokenStorage
     */
    private TokenStorage $tokenStorage;

    public function __construct(TrackRepository $repository, TokenStorage $tokenStorage)
    {
        $this->repository = $repository;
        $this->tokenStorage = $tokenStorage;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return TrackModel::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): TrackModel
    {
        $user = $this->tokenStorage->getToken()->getUser();

        $track = $this->repository->find($id);
        if (!$track) {
            throw new NotFoundHttpException();
        }
        $model = (new TrackModel())->setId($track->getId())->setName($track->getName());
        foreach ($track->getLapTimes() as $lapTime) {
            $lapModel = (new LapTimeModel())
                ->setId($lapTime->getId())
                ->setCar($lapTime->getCar())
                ->setDriverName($lapTime->getDriver() instanceof Driver ? $lapTime->getDriver()->getFullname() : '')
                ->setMilliseconds($lapTime->getMilliseconds());
            if ($lapTime instanceof RealLapTime) {
                $model->addLapTime($lapModel);
                continue;
            }
            if ($user === $lapTime->getDriver()) {
                $lapModel->setGame($lapTime->getGame());
                $model->addLapTime($lapModel);
            }
        }

        return $model;
    }
}