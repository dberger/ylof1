<?php


namespace App\Fixtures\Stub;


final class F12020TracksProvider
{
    const TRACKS = [
        'Australia – Melbourne Grand Prix Circuit',
        'Bahrain – Bahrain International Circuit',
        'Vietnam – Hanoi Circuit',
        'China – Shanghai International Circuit',
        'The Netherlands – Circuit Zandvoort',
        'Spain – Circuit de Barcelona-Catalunya',
        'Monaco – Circuit de Monaco',
        'Azerbaijan – Baku City Circuit',
        'Canada – Circuit Gilles-Villeneuve',
        'France – Circuit Paul Ricard',
        'Austria – Spielberg',
        'Britain – Silverstone Circuit',
        'Hungary – Hungaroring',
        'Belgium – Circuit De Spa-Francorchamps',
        'Italy – Autodromo Nazionale Monza',
        'Singapore – Marina Bay Street Circuit',
        'Russia – Sochi Autodrom',
        'Japan – Suzuka International Racing Course',
        'USA – Circuit of The Americas',
        'México – Autódromo Hermanos Rodríguez',
        'Brazil – Autódromo José Carlos Pace',
        'Abu Dhabi – Yas Marina Circuit',
    ];
}