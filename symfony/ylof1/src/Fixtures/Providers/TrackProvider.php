<?php


namespace App\Fixtures\Providers;


use App\Entity\Track\Track;
use App\Repository\TrackRepository;
use Faker\Generator;
use Faker\Provider\Base;

class TrackProvider extends Base
{
    /**
     * @var TrackRepository
     */
    private $trackRepository;

    public function __construct(Generator $generator, TrackRepository $trackRepository)
    {
        $this->trackRepository = $trackRepository;
        parent::__construct($generator);
    }

    public function getTrack(string $trackName): ?Track
    {
        return $this->trackRepository->findOneByName($trackName);
    }
}