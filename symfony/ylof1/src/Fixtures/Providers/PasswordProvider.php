<?php


namespace App\Fixtures\Providers;


use App\Entity\Driver\Driver;
use App\Entity\Track\Track;
use App\Repository\TrackRepository;
use Faker\Generator;
use Faker\Provider\Base;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordProvider extends Base
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    public function __construct(Generator $generator, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
        parent::__construct($generator);
    }

    public function encodePassword(Driver $driver, string $password): ?string
    {
        return $this->userPasswordEncoder->encodePassword($driver, $password);
    }
}