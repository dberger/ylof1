<?php

namespace App\Entity\Track;

use App\Entity\LapTime\LapTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\TrackRepository;


/**
 * @ORM\Entity(TrackRepository::class)
 * @ORM\Table(name="track",
 *     options={"comment":"Table of tracks"}
 * )
 */
class Track
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Track id"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Track name"})
     */
    #[Assert\NotBlank]
    private ?string $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LapTime\LapTime", mappedBy="track")
     */
    private Collection $lapTimes;

    public function __construct()
    {
        $this->lapTimes = new ArrayCollection();
    }


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Track
     */
    public function setId(?int $id): Track
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Track
     */
    public function setName(?string $name): Track
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Collection|LapTime[]
     */
    public function getLapTimes(): Collection
    {
        return $this->lapTimes;
    }

    /**
     * @param Collection $lapTimes
     * @return Track
     */
    public function setLapTimes(Collection $lapTimes): Track
    {
        $this->lapTimes = $lapTimes;
        return $this;
    }
}
