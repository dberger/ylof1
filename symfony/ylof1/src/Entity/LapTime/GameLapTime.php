<?php


namespace App\Entity\LapTime;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class GameLapTime extends LapTime
{
    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Game used for this laptime"})
     */
    #[Assert\NotBlank]
    private ?string $game;

    /**
     * @return string|null
     */
    public function getGame(): ?string
    {
        return $this->game;
    }

    /**
     * @param string|null $game
     * @return GameLapTime
     */
    public function setGame(?string $game): GameLapTime
    {
        $this->game = $game;
        return $this;
    }
}
