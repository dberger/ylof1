<?php


namespace App\Entity\LapTime;

use App\Entity\Driver\Driver;
use App\Entity\Track\Track;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="laptime",
 *     options={"comment":"Table of laptimes"}
 * )
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "base" = "LapTime",
 *     "game" = "GameLapTime",
 *     "real" = "RealLapTime",
 *     })
 */
abstract class LapTime
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Time id"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="integer", length=255, options={"comment":"Time in milliseconds"})
     */
    #[Assert\NotBlank]
    private ?string $milliseconds;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Car used for this laptime"})
     */
    #[Assert\NotBlank]
    private ?string $car;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Driver\Driver", inversedBy="lapTimes")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    #[Assert\NotBlank]
    private Driver $driver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Track\Track", inversedBy="lapTimes")
     * @ORM\JoinColumn(name="track_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    #[Assert\NotBlank]
    private Track $track;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return LapTime
     */
    public function setId(?int $id): LapTime
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMilliseconds(): ?string
    {
        return $this->milliseconds;
    }

    /**
     * @param string|null $milliseconds
     * @return LapTime
     */
    public function setMilliseconds(?string $milliseconds): LapTime
    {
        $this->milliseconds = $milliseconds;
        return $this;
    }

    /**
     * @return Driver
     */
    public function getDriver(): Driver
    {
        return $this->driver;
    }

    /**
     * @param Driver $driver
     * @return LapTime
     */
    public function setDriver(Driver $driver): LapTime
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * @return Track
     */
    public function getTrack(): Track
    {
        return $this->track;
    }

    /**
     * @param Track $track
     * @return LapTime
     */
    public function setTrack(Track $track): LapTime
    {
        $this->track = $track;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCar(): ?string
    {
        return $this->car;
    }

    /**
     * @param string|null $car
     * @return LapTime
     */
    public function setCar(?string $car): LapTime
    {
        $this->car = $car;
        return $this;
    }
}
