<?php


namespace App\Entity\Driver;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @UniqueEntity("email")
 */
class LocalDriver extends Driver implements UserInterface
{
    const SALT = 'ylof1_21032021';

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Local driver mail"})
     */
    #[Assert\Email]
    private ?string $email;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Local driver password"})
     */
    private ?string $password;

    /**
     * @ORM\Column(type="json", length=255, options={"comment":"Symfony roles"})
     */
    private array $roles = [];

    public function getRoles() : array
    {
        $this->roles[] = 'ROLE_USER';

        return array_unique($this->roles);
    }

    public function getPassword() : ?string
    {
        return $this->password;
    }

    public function getSalt() : string
    {
        return self::SALT . $this->email;
    }

    public function getUsername() : ?string
    {
        return $this->email;
    }

    public function eraseCredentials() : void
    {
        $this->password = null;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return LocalDriver
     */
    public function setEmail(?string $email): LocalDriver
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param string|null $password
     * @return LocalDriver
     */
    public function setPassword(?string $password): LocalDriver
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param array $roles
     * @return LocalDriver
     */
    public function setRoles(array $roles): LocalDriver
    {
        $this->roles = $roles;
        return $this;
    }
}
