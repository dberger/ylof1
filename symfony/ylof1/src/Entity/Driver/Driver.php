<?php

namespace App\Entity\Driver;

use App\Entity\LapTime\LapTime;
use App\Repository\DriverRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=DriverRepository::class)
 * @ORM\Table(name="driver",
 *     options={"comment":"Table of drivers"}
 * )
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "base" = "Driver",
 *     "local" = "LocalDriver",
 *     "real" = "RealDriver",
 *     })
 */
abstract class Driver
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Driver id"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Driver firstname"})
     */
    #[Assert\NotBlank]
    protected ?string $firstName;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Driver lastname"})
     */
    #[Assert\NotBlank]
    protected ?string $lastName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LapTime\LapTime", mappedBy="driver")
     */
    private Collection $lapTimes;

    public function __construct()
    {
        $this->lapTimes = new ArrayCollection();
    }

    public function getFullname(): string
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Driver
     */
    public function setId(?int $id): Driver
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return Driver
     */
    public function setFirstName(?string $firstName): Driver
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return Driver
     */
    public function setLastName(?string $lastName): Driver
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return ArrayCollection|Collection|LapTime[]
     */
    public function getLapTimes(): ArrayCollection|Collection
    {
        return $this->lapTimes;
    }
}
