<?php

namespace App\Model;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\LapTime\LapTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

#[ApiResource(
collectionOperations:
[
    'post',
],
        itemOperations: [
    'put',
    'delete'
]
)]

class DriverModel
{
    private ?string $id;

    #[Assert\NotBlank]
    protected ?string $firstName;

    #[Assert\NotBlank]
    protected ?string $lastName;

    /**
     * @var Collection|LapTime[]
     */
    private Collection $lapTimes;

    public function __construct()
    {
        $this->lapTimes = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return DriverModel
     */
    public function setId(?string $id): DriverModel
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return DriverModel
     */
    public function setFirstName(?string $firstName): DriverModel
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return DriverModel
     */
    public function setLastName(?string $lastName): DriverModel
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function addLapTime(LapTimeModel $lapTime): TrackModel
    {
        if (!$this->lapTimes->contains($lapTime)) {
            $this->lapTimes->add($lapTime);
        }

        return $this;
    }

    public function removeLapTime(LapTimeModel $lapTime): TrackModel
    {
        if ($this->lapTimes->contains($lapTime)) {
            $this->lapTimes->removeElement($lapTime);
        }

        return $this;
    }

    /**
     * @return LapTimeModel[]|Collection
     */
    public function getLapTimes()
    {
        return $this->lapTimes;
    }
}