<?php

namespace App\Model;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\LapTime\LapTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

#[ApiResource(
collectionOperations:
[
    'get',
    'post',
],
        itemOperations: [
    'get',
    'put',
    'delete'
]
)]

class TrackModel
{
    private ?string $id;

    #[Assert\NotBlank]
    private ?string $name;

    /**
     * @var Collection|LapTime[]
     */
    private Collection $lapTimes;

    public function __construct()
    {
        $this->lapTimes = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return TrackModel
     */
    public function setId(?string $id): TrackModel
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return TrackModel
     */
    public function setName(?string $name): TrackModel
    {
        $this->name = $name;
        return $this;
    }

    public function addLapTime(LapTimeModel $lapTime): TrackModel
    {
        if (!$this->lapTimes->contains($lapTime)) {
            $this->lapTimes->add($lapTime);
        }

        return $this;
    }

    public function removeLapTime(LapTimeModel $lapTime): TrackModel
    {
        if ($this->lapTimes->contains($lapTime)) {
            $this->lapTimes->removeElement($lapTime);
        }

        return $this;
    }

    /**
     * @return LapTimeModel[]|Collection
     */
    public function getLapTimes()
    {
        return $this->lapTimes;
    }
}