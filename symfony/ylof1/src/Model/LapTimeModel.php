<?php


namespace App\Model;


class LapTimeModel
{
    private string $id;
    private string $milliseconds;
    private string $car;
    private string $game;
    private string $driverName;
    private string $trackName;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return LapTimeModel
     */
    public function setId(string $id): LapTimeModel
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getMilliseconds(): string
    {
        return $this->milliseconds;
    }

    /**
     * @param string $milliseconds
     * @return LapTimeModel
     */
    public function setMilliseconds(string $milliseconds): LapTimeModel
    {
        $this->milliseconds = $milliseconds;
        return $this;
    }

    /**
     * @return string
     */
    public function getCar(): string
    {
        return $this->car;
    }

    /**
     * @param string $car
     * @return LapTimeModel
     */
    public function setCar(string $car): LapTimeModel
    {
        $this->car = $car;
        return $this;
    }

    /**
     * @return string
     */
    public function getGame(): string
    {
        return $this->game;
    }

    /**
     * @param string $game
     * @return LapTimeModel
     */
    public function setGame(string $game): LapTimeModel
    {
        $this->game = $game;
        return $this;
    }

    /**
     * @return string
     */
    public function getDriverName(): string
    {
        return $this->driverName;
    }

    /**
     * @param string $driverName
     * @return LapTimeModel
     */
    public function setDriverName(string $driverName): LapTimeModel
    {
        $this->driverName = $driverName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTrackName(): string
    {
        return $this->trackName;
    }

    /**
     * @param string $trackName
     * @return LapTimeModel
     */
    public function setTrackName(string $trackName): LapTimeModel
    {
        $this->trackName = $trackName;
        return $this;
    }
}