<?php

namespace App\Twig;

use App\Services\Vite\ViteLoader;
use Psr\Cache\CacheItemPoolInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ViteAssetExtension extends AbstractExtension
{

    public function __construct(
        private ViteLoader $viteLoader
    )
    {

    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('vite_asset', [$this, 'asset'], ['is_safe' => ['html']])
        ];
    }

    public function asset(string $entry, array $deps) : string {
        return $this->viteLoader->asset($entry, $deps);
    }

}