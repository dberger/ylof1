<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    #[Route('/track/{id}', name: 'track_id', requirements: ['id' => "\d+"])]
    public function indexAction() : Response{
        return $this->render('base.html.twig');
    }
}