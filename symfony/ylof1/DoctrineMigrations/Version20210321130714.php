<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Fixtures\Stub\F12020TracksProvider;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add F1 2020 tracks
 */
final class Version20210321130714 extends AbstractMigration
{

    public function getDescription() : string
    {
        return 'Add F1 2020 tracks';
    }

    public function up(Schema $schema) : void
    {
        foreach (F12020TracksProvider::TRACKS as $key => $track){
            $key++;
            $this->addSql("INSERT INTO track VALUES ('{$key}', '{$track}')");
        }
    }

    public function down(Schema $schema) : void
    {
        foreach (F12020TracksProvider::TRACKS as $key => $track){
            $key++;
            $this->addSql("DELETE FROM track where id = {$key}");
        }
    }
}
