<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Init structure
 */
final class Version20210321130624 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Init structure';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE driver (id INT AUTO_INCREMENT NOT NULL COMMENT \'Driver id\', first_name VARCHAR(255) NOT NULL COMMENT \'Driver firstname\', last_name VARCHAR(255) NOT NULL COMMENT \'Driver lastname\', discr VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL COMMENT \'Local driver mail\', password VARCHAR(255) DEFAULT NULL COMMENT \'Local driver password\', roles LONGTEXT DEFAULT NULL COMMENT \'Symfony roles(DC2Type:json)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'Table of drivers\' ');
        $this->addSql('CREATE TABLE laptime (id INT AUTO_INCREMENT NOT NULL COMMENT \'Time id\', driver_id INT NOT NULL COMMENT \'Driver id\', track_id INT NOT NULL COMMENT \'Track id\', milliseconds INT NOT NULL COMMENT \'Time in milliseconds\', car VARCHAR(255) NOT NULL COMMENT \'Car used for this laptime\', discr VARCHAR(255) NOT NULL, game VARCHAR(255) DEFAULT NULL COMMENT \'Game used for this laptime\', INDEX IDX_A5D08286C3423909 (driver_id), INDEX IDX_A5D082865ED23C43 (track_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'Table of laptimes\' ');
        $this->addSql('CREATE TABLE track (id INT AUTO_INCREMENT NOT NULL COMMENT \'Track id\', name VARCHAR(255) NOT NULL COMMENT \'Track name\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'Table of tracks\' ');
        $this->addSql('ALTER TABLE laptime ADD CONSTRAINT FK_A5D08286C3423909 FOREIGN KEY (driver_id) REFERENCES driver (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE laptime ADD CONSTRAINT FK_A5D082865ED23C43 FOREIGN KEY (track_id) REFERENCES track (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE laptime DROP FOREIGN KEY FK_A5D08286C3423909');
        $this->addSql('ALTER TABLE laptime DROP FOREIGN KEY FK_A5D082865ED23C43');
        $this->addSql('DROP TABLE driver');
        $this->addSql('DROP TABLE laptime');
        $this->addSql('DROP TABLE track');
    }
}
