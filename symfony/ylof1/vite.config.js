import { defineConfig } from 'vite'
import reactRefresh from '@vitejs/plugin-react-refresh'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [reactRefresh()],
  root: './assets',
  base: '/assets',
  build: {
    manifest: true,
    assetsDir: '',
    outDir: '../public/assets',
    rollupOptions: {
      input: {
        'main.jsx' : './assets/main.jsx'
      }
    }
  }
})
