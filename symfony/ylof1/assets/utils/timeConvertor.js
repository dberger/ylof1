export const formatMs = $ms =>  {
    let s = Math.floor($ms / 1000) % 60;
    const m = Math.floor($ms / 60000) % 60;
    let ms = $ms - (m * 60000) - (s * 1000);

    s = s.toString().padStart('2', '0');
    ms = ms.toString().padStart('3', '0');

    return `${m}:${s}:${ms}`
};