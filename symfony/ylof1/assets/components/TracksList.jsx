import {useFetch} from "../hooks/fetch";
import {Link} from "react-router-dom";
import React, {useContext, useEffect} from "react";
import {ErrorContext} from "../context/ErrorContext";

export const TracksList = () => {
    const {data, fetchError} = useFetch({url: '/api/track_models'});
    const { setError } = useContext(ErrorContext);

    useEffect(() => {
        if(fetchError){
            setError(fetchError)
        }
    }, [fetchError])

    return (
        <div>
            {data && data.map(track =>
                <Link key={track.id} to={`/track/${track.id}`}>
                    <p>{track.name}</p>
                </Link>)}
        </div>
    );
};