import React, {useContext, useEffect} from "react"
import {useParams} from "react-router";
import {useFetch} from "../hooks/fetch";
import {ErrorContext} from "../context/ErrorContext";
import {formatMs} from "../utils/timeConvertor";
import { useHistory } from "react-router-dom";

export const Track = () => {
    let {id} = useParams();
    const {data, fetchError} = useFetch({url: `/api/track_models/${id}`});
    const {setError} = useContext(ErrorContext);
    const history = useHistory();

    useEffect(() => {
        if (fetchError) {
            setError(fetchError)
        }
    }, [fetchError])

    return (
        <>
            <table>
                <thead>
                <tr>
                    <th>Pilote</th>
                    <th>Time</th>
                    <th>Car</th>
                </tr>
                </thead>
                <tbody>
                {data && data.lapTimes.map(lapTime => (
                    <tr>
                        <td>{lapTime.driverName}</td>
                        <td>{formatMs(lapTime.milliseconds)}</td>
                        <td>{lapTime.car}</td>
                    </tr>
                ))}

                </tbody>
            </table>
            <button
                onClick={() => {
                    history.goBack();
                }}
            >
                Go back
            </button>
        </>
    );
};